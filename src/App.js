import React, { useState } from 'react';
import './App.css';
import NavBar from './components/NavBar';
import { Container } from 'react-bootstrap';
import Home from './pages/Home';
import Books from './pages/Books';
import Register from './pages/Register';
import Login from './pages/Login';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';

import NotFound from './pages/NotFound';

import UserContext from './UserContext';
import AddBook from './pages/AddBook';
import SingleProduct from './components/SingleProduct';

function App() {


  const [user, setUser] = useState({ accessToken: localStorage.getItem('accessToken'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
 });
    const unsetUser = () => {
      localStorage.clear();
      setUser({
        accessToken: null,
        isAdmin: null
      })
    } 

  return (
     <UserContext.Provider value={{ user, setUser, unsetUser }}>
      <Router>
        <NavBar/>
        <Container>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/books" component={Books} />
              <Route exact path="/loginuser" component={Login} />
              <Route exact path="/registeruser" component={Register} />
              <Route exact path="/addbook" component={AddBook} />
              <Route exact path='/products/:id' component={SingleProduct} />
              <Route component={NotFound} />
            </Switch>
        </Container>
      </Router>
    </UserContext.Provider>
  );
}

export default App;