import React, { useState, useEffect } from 'react'; //1
import { Modal, Button, Form } from 'react-bootstrap'; //2
import Swal from 'sweetalert2'; //3
import { useHistory } from 'react-router-dom';

export default function UpdateButton ({bookId}) { //4

	const history = useHistory();

	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [author, setAuthor] = useState('');

	const [isActive, setIsActive] = useState(true);

	useEffect(()=>{
		if(title !== '' && description !== '' && author !== '' && price !== 0){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [title, description, price, author])


	function updateBook() {
		fetch(`http://localhost:4000/prod${bookId}/update`,{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				title: title,
				description: description,
				price: price,
				author: author
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: 'Book Created',
					icon: 'success',
					text: 'Successful Book Creation'
				})

				.then(()=>history.go(0))
			} else {
				Swal.fire({
					icon: 'error',
					title: 'Book Creation Failed'
				})
			}
		})
	}

	return( //5
	<>

		<Button variant="primary" onClick={handleShow}>Update</Button>
	
		<Modal show={show} onHide={handleClose}>
        	<Modal.Header closeButton>
         		<Modal.Title>Update a Book</Modal.Title>
        </Modal.Header>

        <Modal.Body>
        	<Form>
        		<Form.Group>
        			<Form.Label>Title:</Form.Label>
        			<Form.Control 
        				type="text"
        				placeholder="Enter Updated Book Title"
        				value={title}
        				onChange={(e)=> setTitle(e.target.value)}
        				required
        			/>
        		</Form.Group>

        		<Form.Group>
        			<Form.Label>Description:</Form.Label>
        			<Form.Control 
        				type="text"
        				placeholder="Enter Updated Description"
        				value={description}
        				onChange={(e)=> setDescription(e.target.value)}
        				required
        			/>
        		</Form.Group>

        		<Form.Group>
        			<Form.Label>Price:</Form.Label>
        			<Form.Control 
        				type="number"
        				value={price}
        				onChange={(e)=> setPrice(e.target.value)}
        				required
        			/>
        		</Form.Group>

        		<Form.Group>
        			<Form.Label>Author:</Form.Label>
        			<Form.Control 
        				type="text"
        				value={author}
        				onChange={(e)=> setAuthor(e.target.value)}
        				required
        			/>
        		</Form.Group>
        	</Form>
        </Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          
          { 
          	isActive ? 
				<Button type="submit" variant="primary" onClick={updateBook}>Save Changes</Button>
					:
				<Button type="submit" variant="primary" disabled>Save Changes</Button>
			}

        </Modal.Footer>
      </Modal>
     </>
	)

}