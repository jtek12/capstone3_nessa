import React, { useState, useContext } from 'react';
import { Card, Button, CardColumns } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Highlights from '../components/Highlights';
import PropTypes from 'prop-types';
import SingleProduct from '../components/SingleProduct';
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Book ({book, bookId}) {

	const [isOpen, setIsOpen] = useState(true)

	const { title, description, price, author } = book;

	const history = useHistory();

	const { user } = useContext(UserContext);

function seeDetails (bookId){

	fetch('http://localhost:4000/products/', {
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)
		if(!user.isAdmin){
			
			history.push(`/products/${bookId}`)

		} else {
			history.push('/')
		}

	})
}

	return (

	<CardColumns className='mt-3'>

		 <Card className='cardBooks'>
			<Card.Body>
		      <Card.Title className='mt-3'>{title}</Card.Title>
		      <Card.Subtitle className="mb-2 text-muted">by {author}</Card.Subtitle>
		      {isOpen ? <Button variant="primary" onClick={()=> seeDetails(bookId)}> See More Details </Button> : <Button variant="primary" onClick={seeDetails} disabled> See More Details </Button>}
		    </Card.Body>
		  </Card>
	</CardColumns>


	)

};

Book.propTypes = {
	
	book: PropTypes.shape({
		title: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.string.isRequired,
		author: PropTypes.number.isRequired
	})
}




