import React from 'react';
import { Row, Col, Jumbotron} from 'react-bootstrap';

export default function Highlights () {
	return(

	<Row className='highlights'>
		<Col>
			<Jumbotron>
				<h1 className='text-center'>Read for a Cause</h1>
				<p className='font-weight-bold'>Welcome to Book Warriors, your online bookstore for a cause.</p>
				<p>We sell pre-loved books to make a difference.All proceeds of our sales go to select charitable institutions or outreach programs so every time you open a book from us, you'll be reminded of the impact you have on individuals, families, and communities. We started by donating our own books and eventually, more and more people wanted to join.</p>
				<h6 className='font-weight-bold'><a href="/registeruser">Be a book warrior now!</a></h6>
			</Jumbotron>
		</Col>

		<Col>
			<Jumbotron className="image">
			</Jumbotron>
		</Col>
	</Row>

		)

}
