import React from 'react';
import {Jumbotron, Button, Row, Col} from 'react-bootstrap';

export default function Banner() {
	return(

	<Row>
		<Col>
			<Jumbotron className="banner">
				<h1 className="font-weight-bold mb-2 mt-4">Book Warriors</h1>
				<p>Turning Reading Into An Act of Kindness, One Book At A Time.</p>
				<Button className="bg-primary" href="/books"> Shop now! </Button>
			</Jumbotron>
		</Col>
	</Row>



		)
}