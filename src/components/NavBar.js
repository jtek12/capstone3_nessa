import React, { Fragment, useContext} from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink, useHistory } from 'react-router-dom';
import UserContext from '../UserContext'

export default function NavBar() {

  const history = useHistory();

  const { user, setUser, unsetUser } = useContext(UserContext);

  const logout = () => {
    unsetUser();
    setUser({ accessToken: null });
    history.push('/loginuser')
  }

  let rightNav = ( user.accessToken !== null ) ?

  user.isAdmin
  ?
  <Fragment>
    <Nav.Link as={NavLink} to="/addbook">Add a Product</Nav.Link>
    <Nav.Link onClick={logout}>Logout</Nav.Link>
  </Fragment>

  :

  <Fragment>
    <Nav.Link onClick={logout}>Logout</Nav.Link>
  </Fragment>
  :
  <Fragment>
    <Nav.Link as={NavLink} to="/loginuser">Login</Nav.Link>
    <Nav.Link as={NavLink} to="/registeruser">Register Here</Nav.Link>
  </Fragment>

  return(
    
  <Navbar bg="dark" variant="dark" className="nav" expand="lg">
    <Navbar.Brand as={Link} to="/">Book Warriors</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mr-auto">
        <Nav.Link as={NavLink} to="/">Home</Nav.Link>
        <Nav.Link as={NavLink} to="/books">Products</Nav.Link>
      </Nav>
      <Nav className="ml-auto">
        {rightNav}
      </Nav>
    </Navbar.Collapse>
  </Navbar>

    )
}