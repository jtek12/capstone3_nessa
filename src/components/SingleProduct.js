import React, { useEffect, useState, useContext } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Book from '../components/Book';

export default function SingleProduct ({ match }) {

const [product, setProduct] = useState([]);
const [ buyNow, setBuyNow] = useState(true)
const { user } = useContext(UserContext);

	/* 
	useEffect(() => {
		if (user.isAdmin === true) {
		setBuyNow(false)
		}
	}, [])*/

	function buy () {

	fetch(`http://localhost:4000/products/${match.params.id}`, {
		headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
		}
	})
		.then(res => res.json())
		.then(data => {
		setProduct(data);
	})

	}

	return (

		<Row>
			<Col>
				<Card className="cardCourse">
					<Card.Body>
						<Card.Title>
						<h4>{product.title}</h4>
						</Card.Title>
						<Card.Text>
						<h6 className="font-weight-bold">Description:</h6>
						<p>
						{product.description}
						</p>

						<h6 className="font-weight-bold">Price:</h6>
						<p>Php {product.price}</p>

						<h6 className="font-weight-bold">Author:</h6>
						<p>{product.author}</p>
						</Card.Text>
						{buyNow ? <Button variant="primary" onClick={()=> buy(match)}> Buy Now </Button> : <Button variant="primary" onClick={buy} disabled> Buy Now </Button>}
					</Card.Body>
				</Card>
			</Col>
		</Row>

)

}