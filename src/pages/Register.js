import React, { useState, useEffect, useContext } from 'react';
import { Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register () {

	const history = useHistory();

	const { user } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [verifyPassword, setVerifyPassword] = useState('');

	const [registerButton, setRegisterButton] = useState(false)

	useEffect(() => {

		if((email !== '' && password !== '' && verifyPassword !== '') && (password === verifyPassword)) {
				setRegisterButton(true)

		} else {

		setRegisterButton(false)
		}

	}, [email, password, verifyPassword])

	function registerUser(e){
	
		e.preventDefault();

		fetch('http://localhost:4000/users/registeruser/', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			Swal.fire({
				title: 'Successful Registration',
				icon: 'success',
				text: 'Thank you for registering!'
			})

			history.push('/loginuser')
		})

		setEmail('');
		setPassword('');
		setVerifyPassword('');
	}

	if (user.accessToken !== null){
		return <Redirect to="/" />
	}

	return (


		<Form onSubmit={(e) => registerUser(e)}>

			<h2 class='mt-5 mb-3 font-weight-bold'>Register Here</h2>
			<Form.Group controlId="userEmail" class='mt-4 mb-3'>
				<Form.Label>Email Address:</Form.Label>
				<Form.Control type="email" placeholder="Enter your email" value={email} onChange={e => setEmail(e.target.value)} required/>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter your password" value={password} onChange={e => setPassword(e.target.value)} required/>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password:</Form.Label>
				<Form.Control type="password" placeholder="Verify your password" value={verifyPassword} onChange={e => setVerifyPassword(e.target.value)} required/>
			</Form.Group>

			{registerButton ? <Button variant="primary" type="submit">Submit</Button> : <Button variant="primary" type="submit" disabled>Submit</Button>}

			<p className='mt-3'>Already have an account? Click <a href="/loginuser">here</a> to login.</p>

		</Form>
	)

}