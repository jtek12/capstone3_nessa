import React from 'react';
import { Link } from 'react-router-dom';
import { Container } from 'react-bootstrap';

export default function notFound () {

	return(

	<div>
		<h1>Page Not Found</h1>
		<Link to="/">
     		 Go to home page.
   		</Link>
	</div>
		)
}