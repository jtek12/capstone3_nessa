import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';

export default function AddBook(){

	const history = useHistory();

	const { user } = useContext(UserContext);
	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const [author, setAuthor] = useState('');
	const [price, setPrice] = useState(0);

	const [isActive, setIsActive] = useState(true);

	useEffect(()=>{
		if(title !== '' && description !== '' && price !== 0 && author !== ''){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [title, description, author, price])


	function addBook(e){
		e.preventDefault();

		fetch('http://localhost:4000/products/add', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				title: title,
				description: description,
				price: price,
				author: author
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire({
					title: 'Book Created',
					icon: 'success'
				})
				history.push('/books')
			}else{
				Swal.fire({
					title: 'Book Creation Failed',
					icon: 'error'
				})
			}
		})

		setTitle('');
		setDescription('');
		setAuthor('');
		setPrice(0);
	}


	return(
		<>
			<h1 class='mt-5'>Create A New Product</h1>
			<Form onSubmit={ e => addBook(e)}>
				<Form.Group>
					<Form.Label>Book Title:</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter the title of the book"
						value={title}
						onChange={(e) => setTitle(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Description:</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter the description of the book"
						value={description}
						onChange={(e) => setDescription(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Author:</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter the name of the author"
						value={author}
						onChange={(e) => setAuthor(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Price:</Form.Label>
					<Form.Control
						type="number"
						value={price}
						onChange={(e) => setPrice(e.target.value)}
						required
					/>
				</Form.Group>

				{ isActive ? 
					<Button type="submit" variant="primary">Add a Product</Button>
					:
					<Button type="submit" variant="primary" disabled>Add a Product</Button>
				}
				
				
			</Form>
		</>
		)
}
