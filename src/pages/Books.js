import React, { useContext, useEffect, useState }from 'react';
import Book from '../components/Book';
import UserContext from '../UserContext';
import { Table } from 'react-bootstrap';
import DeleteButton from '../components/DeleteButton';
import UpdateButton from '../components/UpdateButton';

export default function Books () {

	const [adminBooks, setAdminBooks] = useState([]);
	const [allBooks, setAllBooks] = useState([]);

	const { user } = useContext(UserContext);
	console.log(user)

		useEffect(() => {

		if (!user.isAdmin) {

			fetch("http://localhost:4000/products/", {
				headers: { 
					Authorization: `Bearer $(user.accessToken)`
				}
			})
			.then(response => response.json())
			.then(userBooks =>{
				console.log(userBooks);

				setAllBooks(userBooks.map(books => {
					return (

						<Book key={books._id} book={books} bookId={books._id}/>
					)
				}));	

			});


		} else {

			fetch("http://localhost:4000/products/", {
				headers: { 
					'Authorization': `Bearer $(user.accessToken)` 
				}
			})
			.then(res => res.json())
			.then(activeBooks => {
				console.log(activeBooks)
				setAdminBooks(activeBooks.map(book => {
				
					return (
						<tr key={book._id}>
							<td>{book._id}</td>
							<td>{book.title}</td>
							<td>{book.description}</td>
							<td>{book.author}</td>
							<td>{book.price}</td>
							<td className={book.isActive ? "text-success" : "text-danger"}>{book.isActive ? "Active" : "Inactive"}</td>
							<td><DeleteButton bookId={book._id} /></td>
							<td><UpdateButton bookId={book._id} /></td>
						</tr>
					)

				}));
			})

		}
		
	}, [])

	if (!user.isAdmin) {

		return (

			[allBooks]	
		)

	} else {

		return (

		<>
				<h1 className="text-center mb-5 mt-5"><strong>Book Dashboard</strong></h1>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>ID</th>
							<th>Title</th>
							<th>Description</th>
							<th>Author</th>
							<th>Price</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
							{adminBooks}
					</tbody>
				</Table>
		</>

		)
	}

}