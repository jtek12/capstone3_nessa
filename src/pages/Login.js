import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Redirect, useHistory } from 'react-router-dom';


export default function Login () {

	const history = useHistory();

	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [loginButton, setLoginButton] = useState(false)

	useEffect(() => {

		if(email !== '' && password !== ''){

			setLoginButton(true)
		
		} else {

			setLoginButton(false)
		}

	}, [email, password])

	function login (e) {

		e.preventDefault();

		//API.3
		fetch('http://localhost:4000/users/loginuser', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => { 
			console.log(data)

			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken);
				setUser({ accessToken: data.accessToken });

				Swal.fire({
					title: "Yay.",
					icon: "success",
					text: "Thank you for logging into Book Warriors."
				});

		fetch('http://localhost:4000/users/details', {
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data.isAdmin === true){
				localStorage.setItem('email', data.email);
				localStorage.setItem('isAdmin', data.isAdmin);

				setUser({
					emailId: data.userId,
					isAdmin: data.isAdmin
				})

				history.push('/books')

			} else {

				history.push('/')

			}

		})

			}  else {

				Swal.fire({
					title: "Oops.",
					icon: "error",
					text: "Something went wrong, check your credentials."
				})
			}

			setEmail('');
			setPassword('');

		})
	}

	if (user.accessToken !== null) {
		return <Redirect to="/" />
	}

	return(

		<Form onSubmit={(e) => login(e)}> 
			<h2 class='mt-5 mb-3 font-weight-bold'>Log In</h2>
			<Form.Group controlId="userEmail" class='mt-4 mb-3'>
				<Form.Label>Email Address:</Form.Label>
				<Form.Control type="email" placeholder="Enter your email" value={email} onChange={e => setEmail(e.target.value)} required/>
				<Form.Text className="text-muted">
					Enter your email
				</Form.Text>
			</Form.Group>

		<Form.Group controlId="password1">
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter your password" value={password} onChange={e => setPassword(e.target.value)} required/>
		</Form.Group>
			{loginButton ? <Button variant="primary" type="submit">Login Here</Button> : <Button variant="primary" type="submit" disabled>Login Here</Button>}

		<p class='mt-3'>Don't have an account yet? Click <a href="/registeruser">here</a> to register.</p>
		</Form>

	)
}